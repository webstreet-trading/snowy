<?php
/**
 * Default Blog output
 *
 * Override this template by copying it to yourtheme/ws-view/modules/blog/default.php
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$post_format = et_pb_post_format();

$thumb = '';

$width = 'on' === $fullwidth ? 1080 : 400;
$width = (int) apply_filters('et_pb_blog_image_width', $width);

$height = 'on' === $fullwidth ? 675 : 250;
$height = (int) apply_filters('et_pb_blog_image_height', $height);
$classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
$titletext = get_the_title();
$thumbnail = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
$thumb = $thumbnail['thumb'];

$no_thumb_class = '' === $thumb || 'off' === $show_thumbnail ? ' et_pb_no_thumb' : '';

if (in_array($post_format, array('video', 'gallery'))) {
    $no_thumb_class = '';
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('et_pb_post clearfix' . $no_thumb_class . $overlay_class); ?>>

    <?php
    et_divi_post_format_content();

    if (!in_array($post_format, array('link', 'audio', 'quote')) || post_password_required($post)) {
        if ('video' === $post_format && false !== ( $first_video = et_get_first_video() )) :
            $video_overlay = has_post_thumbnail() ? sprintf(
                    '<div class="et_pb_video_overlay" style="background-image: url(%1$s); background-size: cover;">
                    <div class="et_pb_video_overlay_hover">
                        <a href="#" class="et_pb_video_play"></a>
                    </div>
                </div>',
                $thumb
            ) : '';

            printf(
                '<div class="et_main_video_container">
                    %1$s
                    %2$s
                </div>',
                $video_overlay,
                $first_video
            );
        elseif ('gallery' === $post_format) :
            et_pb_gallery_images('slider');
        elseif ('' !== $thumb && 'on' === $show_thumbnail) :
            if ('on' !== $fullwidth) {
                echo '<div class="et_pb_image_container">';
            }
            ?>
                <a href="<?php esc_url(the_permalink()); ?>" class="entry-featured-image-url">
                <?php print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height); ?>
                <?php
                if ('on' === $use_overlay) {
                    echo $overlay_output;
                }
                ?>
                </a>
                <?php
                if ('on' !== $fullwidth) {
                    echo '</div> <!-- .et_pb_image_container -->';
                }
        endif;
    }
    ?>

    <?php if ('off' === $fullwidth || !in_array($post_format, array('link', 'audio', 'quote')) || post_password_required($post)) { ?>
        <?php if (!in_array($post_format, array('link', 'audio')) || post_password_required($post)) { ?>
        <<?php echo $processed_header_level; ?> class="entry-title"><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></<?php echo $processed_header_level; ?>>
    <?php } ?>

    <?php
    if ( 'on' === $show_author || 'on' === $show_date || 'on' === $show_categories || 'on' === $show_comments ) {
        printf( '<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s %7$s</p>',
            (
                'on' === $show_author
                    ? et_get_safe_localization( sprintf( __( 'by %s', 'et_builder' ), '<span class="author vcard">' .  et_pb_get_the_author_posts_link() . '</span>' ) )
                    : ''
            ),
            (
                ( 'on' === $show_author && 'on' === $show_date )
                    ? ' | '
                    : ''
            ),
            (
                'on' === $show_date
                    ? et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' . esc_html( get_the_date( $meta_date ) ) . '</span>' ) )
                    : ''
            ),
            (
                (( 'on' === $show_author || 'on' === $show_date ) && 'on' === $show_categories)
                    ? ' | '
                    : ''
            ),
            (
                'on' === $show_categories
                    ? get_the_category_list(', ')
                    : ''
            ),
            (
                (( 'on' === $show_author || 'on' === $show_date || 'on' === $show_categories ) && 'on' === $show_comments)
                    ? ' | '
                    : ''
            ),
            (
                'on' === $show_comments
                    ? sprintf( esc_html( _nx( '%s Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder' ) ), number_format_i18n( get_comments_number() ) )
                    : ''
            )
        );
    }

    echo '<div class="post-content">';
    global $et_pb_rendering_column_content;

    $post_content = et_strip_shortcodes(et_delete_post_first_video(get_the_content()), true);

    $et_pb_rendering_column_content = true;

    if ('on' === $show_content) {
        global $more;

        // page builder doesn't support more tag, so display the_content() in case of post made with page builder
        if (et_pb_is_pagebuilder_used(get_the_ID())) {
            $more = 1;
            echo apply_filters('the_content', $post_content);
        }
        else {
            $more = null;
            echo apply_filters('the_content', et_delete_post_first_video(get_the_content(esc_html__('read more...', 'et_builder'))));
        }
    }
    else {
        echo wpautop(et_delete_post_first_video(strip_shortcodes(truncate_post(270, false, '', true))));
    }

    $et_pb_rendering_column_content = false;

    if ('on' !== $show_content) {
        $more = 'on' == $show_more ? sprintf(' <a href="%1$s" class="more-link" >%2$s</a>', esc_url(get_permalink()), esc_html__('read more', 'et_builder')) : '';
        echo $more;
    }

    echo '</div>';
    ?>
    <?php } // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery' ?>

</article> <!-- .et_pb_post -->