<?php

// run includes
include('ws/divi/functions.php');

/**
 * Set styles and javascript
 */
function ws_enqueue_css_js() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('theme-style', get_stylesheet_directory_uri() . '/assets/css/styles.css', array(), ws('version'));
    
    wp_enqueue_script('theme-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'), ws('version'), true);
}
add_action('wp_enqueue_scripts', 'ws_enqueue_css_js', 99);
add_action( 'login_enqueue_scripts', 'ws_enqueue_css_js', 99);

/* function ws_login_page_styling() { ?>
    <style type="text/css">
        #loginform {
            padding: 4%;
        }
        .et_divi_100_custom_login_page--style-7 .divi-login__input {
            border-radius: 0 !important;
            padding: 0 !important;
        }
        .et_divi_100_custom_login_page--style-7 .divi-login__input .icon {
            
            color: #F285BB !important;
            left: 20px !important;
            
        }
        .et_divi_100_custom_login_page--style-7 .divi-login__input input {
            
            border: solid #F285BB !important;
            border-width: 0 1px 1px 0 !important;            
            
        }
        
        #wp-submit {
            
            border-radius: 0;
            font-size: 22px;
            text-transform: uppercase;
            
        }
    </style>
    <script type="text/javascript">
        document.getElementById('user_login').placeholder = 'Username';
        document.getElementById('user_pass').placeholder = 'Password';
    </script>
//<?php }
//add_action( 'login_enqueue_scripts', 'ws_login_page_styling' ); */


//Use this tomottow
function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style-login.css' );
    wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/style-login.js' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );